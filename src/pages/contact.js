import React from 'react';
import Header from './Header';
import Grid from '@material-ui/core/Grid';
import TopBar from './topbar';
import Navbar from './navbar';

class About extends React.Component{
  render()
  {
    return(
    	<div>

    	<Header />
		<TopBar />
<Grid container spacing={24} >
        <Grid item 
xs={12} style={{marginRight:50, marginLeft:50}}> 

	<h1 align = "center">
		Contact Us
	</h1>
		<h2>
			We appreciate your interest in the Children's Foundation of Technology.  Here are some ways you can contact us.
		</h2>

		<h3>
			Email:
		</h3> 
		<div>
				<a href="url"> Info.cfotsl@gmail.com </a>
		</div>
<h3>
			School Address:
			</h3>
			<div>
			Koya Town Wellington <br />
			Freetown, Sierra Leone

		</div> <br />

		<form method="post" action="mailto: Info.cfotsl@gmail.com">
		<input type="submit" value="Send Email" />
		</form>
</Grid>
</Grid>
<Navbar />
</div>

)
}
}
export default About;