/*
   --------
   import the packages we need
   --------
 */

import React from 'react';
import { connect, Provider } from 'react-redux';
import { createStore, combineReducers, compose } from 'redux';
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import firebase from 'firebase';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './style/theme';
import initialState from './initialState.json';
import './style/main.css';



/*
   --------
   import your pages here
   --------
 */
import HomePage from './pages/home';

import AboutPage from './pages/aboutus';
import PhotosPage from './pages/photos';
import HowtoHelpPage from './pages/HowtoHelp';
import ContactPage from './pages/contact';
import NewsPage from './pages/news';
import PartnersPage from './pages/partners';
import Videos from './pages/videos';

import TermsofService from './pages/ToS';
import PrivacyPolicy from './pages/PP';
import BlogPage from './pages/blog';
import Articles from './components/Article';
import DonationPage from './pages/donations';
import homenews from './pages/homenews';
import homeaboutus from './pages/homeaboutus';
import stats from './pages/stats';


/*
   --------
   configure everything
   --------
 */

const firebaseConfig = {
    /*
       --------
       REPLACE WITH YOUR FIREBASE CREDENTIALS
       --------
     */
    apiKey: "AIzaSyAc_q2GfZSdtql6W76zbP6J3LG3s6OD3e4",
    authDomain: "cooper-csesg-project-template.firebaseapp.com",
    databaseURL: "https://cooper-csesg-project-template.firebaseio.com",
    projectId: "cooper-csesg-project-template",
    storageBucket: "cooper-csesg-project-template.appspot.com",
    messagingSenderId: "871061605870"
};

// react-redux-firebase config
const rrfConfig = {
    userProfile: 'users',
};

// Initialize firebase instance
firebase.initializeApp(firebaseConfig);









/*
   --------
   setup redux and router
   --------
 */


const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase, rrfConfig)
)(createStore);

// Add firebase to reducers
const rootReducer = combineReducers({
    firebase: firebaseReducer
});

const store = createStoreWithFirebase(rootReducer, initialState);


const ConnectedRouter = connect()(Router);



export default class App extends React.Component{
    render(){
	return(
	    <MuiThemeProvider theme={theme}>
		<Provider store={store}>
		    <ConnectedRouter>
			<div>
			  <Route exact path="/" component={HomePage} />
			  <Route exact path="/photos" component={PhotosPage} />
  			<Route exact path="/about" component={AboutPage} />
				<Route exact path="/how_to_help" component={HowtoHelpPage} />
				<Route exact path="/contact" component={ContactPage} />
				<Route exact path="/news" component={NewsPage} />
				<Route exact path="/partners" component={PartnersPage} />
        <Route exact path="/videos" component={Videos} />
        <Route exact path="/ToS" component={TermsofService} />
        <Route exact path="/PP" component={PrivacyPolicy} />
        <Route exact path="/blog" component={BlogPage} />
        <Route exact path="/blog/:id" component={Articles} />
        <Route exact path="/donations" component={DonationPage} />
        <Route exact path="homenews" component={homenews}/>
        <Route exact path="homeaboutus" component={homeaboutus}/>
			</div>
		    </ConnectedRouter>
		</Provider>
	    </MuiThemeProvider>
	);
    }
}
